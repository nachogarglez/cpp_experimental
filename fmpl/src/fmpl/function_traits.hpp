/*-----------------------------------------------------------------------------

function_traits
Split a function type into its signature components

For a given callable:
* arg_tuple  = tuple equivalent to callable's argument types
* return_types = callable return type
-----------------------------------------------------------------------------*/

#pragma once
#include <tuple>
#include "config.h"

#if CPP_STANDARD < CPP_STANDARD_14
#error Need C++14 or higher
#endif

namespace fmpl {

// Primary template. Does nothing, fails.
template <typename Type> struct function_traits;

// Case reference to a function
template <typename returnValue, typename... callableArgs>
struct function_traits<returnValue (&)(callableArgs...)> {
  using arg_tuple = std::tuple<callableArgs...>;
  using return_type = returnValue;
};

// Case type of function
template <typename returnValue, typename... callableArgs>
struct function_traits<returnValue(callableArgs...)> {
  using arg_tuple = std::tuple<callableArgs...>;
  using return_type = returnValue;
};

} // namespace fmpl