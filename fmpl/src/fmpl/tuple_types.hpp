/*-----------------------------------------------------------------------------

Functions related with Tuple's types


get_tuple_type_position
  In a tuple, get the position of a the first matching type

tuple_has_type
  ::value ==  std::true_type if the tuple contains the specified type
-----------------------------------------------------------------------------*/

#pragma once
#include "config.h"
#include "type_position.hpp"
#include <tuple>
#include <type_traits>

#if CPP_STANDARD < CPP_STANDARD_17
#error Need C++17 or higher
#endif

namespace fmpl {

//------------------------------
// get_tuple_type_position
//------------------------------

template <typename Type, typename Tuple> struct get_tuple_type_position;
template <typename Type, typename... tupleArgs>
struct get_tuple_type_position<Type, std::tuple<tupleArgs...>>
    : get_type_position<Type, tupleArgs...> {};

template <typename Type, typename Tuple> struct tuple_has_type;
template <typename Type, typename... tupleArgs>
struct tuple_has_type<Type, std::tuple<tupleArgs...>>
    : std::disjunction<std::is_same<Type, tupleArgs>...> {};

} // namespace fmpl