/*-----------------------------------------------------------------------------

Type-Position related functions

get_type_position:
  Get position of a type in a type list

get_type_by_position:
  In a type list, get the type corresponding to a given position

-----------------------------------------------------------------------------*/

#pragma once
#include <tuple>
#include <type_traits>
#include "config.h"

#if CPP_STANDARD < CPP_STANDARD_14
#error Need C++14 or higher
#endif

namespace fmpl {
//------------------------------
// get_type_position
//------------------------------

template <typename T, typename... Ts> struct get_type_position {
  static_assert(sizeof...(Ts) == 1, "Type not found");
};

template <typename T, typename... Ts>
struct get_type_position<T, T, Ts...> : std::integral_constant<std::size_t, 0> {
};
template <typename T, typename Tail, typename... Ts>
struct get_type_position<T, Tail, Ts...>
    : std::integral_constant<std::size_t,
                             1 + get_type_position<T, Ts...>::value> {};

//------------------------------
// get_type_by_position
//------------------------------

template <std::size_t N, typename T, typename... types>
struct get_type_by_position {
  static_assert(N <= sizeof...(types), "Out of range");
  using type = typename get_type_by_position<N - 1, types...>::type;
};

template <typename T, typename... types>
struct get_type_by_position<0, T, types...> {
  using type = T;
};

} // namespace fmpl