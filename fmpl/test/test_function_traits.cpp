#include "fmpl/function_traits.hpp"
// This tells Catch to provide a main() - only do this in one cpp file
#define CATCH_CONFIG_MAIN
// Testging framework
#include "catch2/catch.hpp"

#include <tuple>

using namespace fmpl;

void fnc1(int) {}
char fnc2(int, char) { return char{}; }
char fnc3(int, char, long *) { return char{}; }
int *fnc4() { return nullptr; }

template <typename callable> auto test_function_ref_arg_type(callable &&c) {
  (void)c;
  return typeid(typename function_traits<callable>::arg_tuple).name();
}
template <typename callable> auto test_function_ref_return_type(callable &&c) {
  (void)c;
  return typeid(typename function_traits<callable>::return_type).name();
}

TEST_CASE("Extract arguments as tuple from a reference to free function") {
  REQUIRE(test_function_ref_arg_type(fnc1) == typeid(std::tuple<int>).name());
  REQUIRE(test_function_ref_arg_type(fnc2) ==
          typeid(std::tuple<int, char>).name());
  REQUIRE(test_function_ref_arg_type(fnc3) ==
          typeid(std::tuple<int, char, long *>).name());
  REQUIRE(test_function_ref_arg_type(fnc4) == typeid(std::tuple<>).name());
}

TEST_CASE("Extract arguments as tuple from a type of free function") {
  REQUIRE(typeid(typename function_traits<decltype(fnc1)>::arg_tuple) ==
          typeid(std::tuple<int>));
  REQUIRE(typeid(typename function_traits<decltype(fnc2)>::arg_tuple) ==
          typeid(std::tuple<int, char>));
  REQUIRE(typeid(typename function_traits<decltype(fnc3)>::arg_tuple) ==
          typeid(std::tuple<int, char, long *>));
  REQUIRE(typeid(typename function_traits<decltype(fnc4)>::arg_tuple) ==
          typeid(std::tuple<>));
}

TEST_CASE("Extract return type from a reference to  afree function") {
  REQUIRE(test_function_ref_return_type(fnc1) == typeid(void).name());
  REQUIRE(test_function_ref_return_type(fnc2) == typeid(char).name());
  REQUIRE(test_function_ref_return_type(fnc3) == typeid(char).name());
  REQUIRE(test_function_ref_return_type(fnc4) == typeid(int *).name());
}

TEST_CASE("Extract return type from a type of free function") {
  REQUIRE(typeid(typename function_traits<decltype(fnc1)>::return_type) ==
          typeid(void));
  REQUIRE(typeid(typename function_traits<decltype(fnc2)>::return_type) ==
          typeid(char));
  REQUIRE(typeid(typename function_traits<decltype(fnc3)>::return_type) ==
          typeid(char));
  REQUIRE(typeid(typename function_traits<decltype(fnc4)>::return_type) ==
          typeid(int *));
}
