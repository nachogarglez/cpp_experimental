#include "fmpl/tuple_types.hpp"
// This tells Catch to provide a main() - only do this in one cpp file
#define CATCH_CONFIG_MAIN
// Testging framework
#include "catch2/catch.hpp"

#include <tuple>

using namespace fmpl;

TEST_CASE(
    "In a type std::tuple, get the type corresponding to a given position") {

  REQUIRE(
      get_tuple_type_position<int, std::tuple<int, char, long, float>>::value ==
      0);
  REQUIRE(get_tuple_type_position<char,
                                  std::tuple<int, char, long, float>>::value ==
          1);
  REQUIRE(get_tuple_type_position<long,
                                  std::tuple<int, char, long, float>>::value ==
          2);
  REQUIRE(get_tuple_type_position<float,
                                  std::tuple<int, char, long, float>>::value ==
          3);
  REQUIRE(get_tuple_type_position<
              int *, std::tuple<int, char, long, float, int *>>::value == 4);
}

TEST_CASE(
    "In a type std::tuple, check if the tuple contains the specified type") {

  REQUIRE(tuple_has_type<int, std::tuple<int, char, long, float>>::value ==
          true);
  REQUIRE(tuple_has_type<char, std::tuple<int, char, long, float>>::value ==
          true);
  REQUIRE(tuple_has_type<long, std::tuple<int, char, long, float>>::value ==
          true);
  REQUIRE(tuple_has_type<float, std::tuple<int, char, long, float>>::value ==
          true);
  REQUIRE(
      tuple_has_type<int *, std::tuple<int, char, long, float, int *>>::value ==
      true);
}

TEST_CASE("In a type std::tuple, check if the tuple NOT contains the specified "
          "type") {

  REQUIRE(tuple_has_type<int *, std::tuple<int, char, long, float>>::value ==
          false);
  REQUIRE(tuple_has_type<char *, std::tuple<int, char, long, float>>::value ==
          false);
  REQUIRE(tuple_has_type<long *, std::tuple<int, char, long, float>>::value ==
          false);
  REQUIRE(tuple_has_type<float *, std::tuple<int, char, long, float>>::value ==
          false);
  REQUIRE(tuple_has_type<double,
                         std::tuple<int, char, long, float, int *>>::value ==
          false);
}
