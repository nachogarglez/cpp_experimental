#include "fmpl/type_position.hpp"
// This tells Catch to provide a main() - only do this in one cpp file
#define CATCH_CONFIG_MAIN
// Testging framework
#include "catch2/catch.hpp"

#include <tuple>

using namespace fmpl;

TEST_CASE("Get the position of a type in a type list") {

  REQUIRE(get_type_position<int, int, char, long>::value == 0);
  REQUIRE(get_type_position<char, int, char, long>::value == 1);
  REQUIRE(get_type_position<long, int, char, long>::value == 2);
  REQUIRE(get_type_position<long, int, char, long>::value == 2);
}

TEST_CASE("In a type list, get the type corresponding to a given position") {

  REQUIRE(typeid(get_type_by_position<0, int, char, long, float>::type) ==
          typeid(int));
  REQUIRE(typeid(get_type_by_position<1, int, char, long, float>::type) ==
          typeid(char));
  REQUIRE(typeid(get_type_by_position<2, int, char, long, float>::type) ==
          typeid(long));
  REQUIRE(typeid(get_type_by_position<3, int, char, long, float>::type) ==
          typeid(float));
  REQUIRE(
      typeid(get_type_by_position<4, int, char, long, float, int *>::type) ==
      typeid(int *));

  
}
