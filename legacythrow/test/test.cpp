#include "legacythrow.hpp"
#include <cstddef>
#include <iostream>

GQuark flu_error_quark(void) {
  return g_quark_from_static_string("flu_error-quark");
}

gboolean legacy_gerror_boolean_fails(int arg1, GError **error, int a) {
  (void)arg1;
  (void)a;
  *error = g_error_new(flu_error_quark(), 69, "Legacy fatal");
  return false;
}
gboolean legacy_gerror_boolean_pass(int arg1, GError **error, int a) {
  (void)error;
  (void)arg1;
  (void)a;
  return true;
}
char *legacy_gerror_pointer_fails(int arg1, GError **error, int a) {
  (void)arg1;
  (void)a;
  *error = g_error_new(flu_error_quark(), 69, "Legacy fatal");
  return nullptr;
}
char *legacy_gerror_pointer_pass(int arg1, GError **error, int a) {
  (void)error;
  (void)arg1;
  (void)a;
  return reinterpret_cast<char *>(uintptr_t(0xCAFECAFE));
}
void *legacy_gerror_pointer2_fails(int arg1, GError **error, int a) {
  (void)arg1;
  (void)a;
  *error = g_error_new(flu_error_quark(), 69, "Legacy fatal");
  return nullptr;
}
void *legacy_gerror_pointer2_pass(int arg1, GError **error, int a) {
  (void)error;
  (void)arg1;
  (void)a;
  return reinterpret_cast<void *>(uintptr_t(0xCAFECAFE));
}
char *legacy_undef_gerror_pointer_fails(int arg1, GError **error, int a) {
  (void)arg1;
  (void)a;
  (void)error;
  return nullptr;
}
char *legacy_no_gerror_pointer_fails(int arg1, int a) {
  (void)arg1;
  (void)a;
  return nullptr;
}
char *legacy_no_gerror_pointer_pass(uintptr_t arg1, int a) {
  (void)arg1;
  (void)a;
  return reinterpret_cast<char *>(uintptr_t(0xCAFECAFE));
}

char *legacy_no_gerror_return_correct_pointer(uintptr_t arg1) {
  return reinterpret_cast<char *>(arg1);
}

GLIB_THROW_IF_FAILS(legacy_gerror_boolean_fails)
GLIB_THROW_IF_FAILS(legacy_gerror_boolean_pass)
GLIB_THROW_IF_FAILS(legacy_gerror_pointer_fails)
GLIB_THROW_IF_FAILS(legacy_gerror_pointer_pass)
GLIB_THROW_IF_FAILS(legacy_gerror_pointer2_fails)
GLIB_THROW_IF_FAILS(legacy_gerror_pointer2_pass)
GLIB_THROW_IF_FAILS(legacy_undef_gerror_pointer_fails)
GLIB_THROW_IF_FAILS(legacy_no_gerror_pointer_fails)
GLIB_THROW_IF_FAILS(legacy_no_gerror_pointer_pass)
GLIB_THROW_IF_FAILS(legacy_no_gerror_return_correct_pointer)

// This tells Catch to provide a main() - only do this in one cpp file
#define CATCH_CONFIG_MAIN
// Testging framework
#include "catch2/catch.hpp"

TEST_CASE("Throws returning a GError* objects") {
  REQUIRE_THROWS_WITH(ThrowIfFail::legacy_gerror_boolean_fails(0, nullptr, 2),
                      "Legacy fatal");
  REQUIRE_THROWS_WITH(ThrowIfFail::legacy_gerror_pointer_fails(0, nullptr, 2),
                      "Legacy fatal");
  REQUIRE_THROWS_WITH(ThrowIfFail::legacy_gerror_pointer2_fails(0, nullptr, 2),
                      "Legacy fatal");
}

TEST_CASE("Throws with undefined GError* objects") {
  REQUIRE_THROWS(ThrowIfFail::legacy_undef_gerror_pointer_fails(0, nullptr, 2));
}

TEST_CASE("Pass without throwing") {
  CHECK_NOTHROW(ThrowIfFail::legacy_gerror_boolean_pass(0, nullptr, 2));
  CHECK_NOTHROW(ThrowIfFail::legacy_gerror_pointer_pass(0, nullptr, 2));
  CHECK_NOTHROW(ThrowIfFail::legacy_gerror_pointer2_pass(0, nullptr, 2));
}

TEST_CASE("No throws and return correct pointer") {
  REQUIRE(ThrowIfFail::legacy_gerror_pointer_pass(0, nullptr, 2) ==
          reinterpret_cast<void *>(uintptr_t(0xCAFECAFE)));
  REQUIRE(ThrowIfFail::legacy_gerror_pointer2_pass(0, nullptr, 2) ==
          reinterpret_cast<void *>(uintptr_t(0xCAFECAFE)));
}

TEST_CASE("Throws without gerror") {
  REQUIRE_THROWS(ThrowIfFail::legacy_no_gerror_pointer_fails(0, 0));
}
  
TEST_CASE("Pass without gerror") {
  CHECK_NOTHROW(ThrowIfFail::legacy_no_gerror_pointer_pass(0xCAFECAFE, 0));
}

TEST_CASE("Return correct poitner") {
  REQUIRE(ThrowIfFail::legacy_no_gerror_return_correct_pointer(0xCAFECAFE) ==
          reinterpret_cast<void *>(uintptr_t(0xCAFECAFE)));
}