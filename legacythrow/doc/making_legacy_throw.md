---
title: "`Making legacy throw`: Reflective Metaprogramming"
document: 0000000
date: 2018-08-21
audience: Fluendo Core Team
author:
  - name: Nacho García
    email: <ngarcia@fluendo.com>
toc: false
---

# Motivation

Recently we implemented automatic exception throwing for our python bindings in Fluendo SDK.
PyObject seems to handle nicely GLib error reports, automatically filling the `GError**` parameter to construct a Python exception from this information.

This previous experience was inspirational for creating this document and its experimental source code. Would be it even possible to achieve a similar automatic wrapping to convert errors into exceptions in ISO C++?
Spoiler: Yes, it is possible by using techniques of "Reflective Metaprogramming".

# Goals

The purpose of this document is not to teach template metaprogramming in deep. That is a quite advanced C++ topic that requires a good previous understanding of C++ template programming; also, that is a topic too huge for a single document.
Our goals in this paper are:

* To help to understand the rationale behind the C++ template metaprogramming and how it can be used to implement logic at build time and to autogenerate code, even if you don't understand completely the template code or if you don't understand it at all.
* To test current status of toolchains and language specifications.
* To create some useful code for the company.
* To evolve our vision about programming by touching new paradigms, e.g, functional code.
* To have fun.

# Code specifications

We are going to create a C++ wrapper that will meet the following specifications:

Given two legacy functions such as:

```cpp
gchar*  my_legacy_one (int param1, int param2)
gchar*  my_legacy_two (int param1, GError** gerror, int param2)
```

those functions have to be wrapped at build time into a corresponding throwing code, and used this way:

```cpp
gchar *result = ThrowIfFail::my_legacy_one(0, 0);
gchar *result = ThrowIfFail::my_legacy_two(0, nullptr, 0);
```

* For `my_legacy_one` an exception has to be thrown if `result == nullptr`.
* For `my_legacy_two` also an exception has to be thrown if `result == nullptr`, but containing the `GError**` information embedded in the exception information.
* `gboolean` return type should be also correctly handled.
* For both cases, the generated runtime code has to be less or equal than the hand-crafted equivalent code, that means zero overhead and zero bloating.

Note: Some of the code will be C++17 or higher. This is not necessary, i.e, this recipe could be implemented also in C++14 or even C++11. However, that would result in more verbose code with mostly repetitive concepts and has little interest. C++17 is more compact and allows us to focus on understanding what kind of things can be done using template metaprogramming.


## Create a perfect wrapper


Our first task will be creating a _perfect_ wrapper for any function. Later we will add more code into this wrapper to handle errors. But, what's a perfect C++ wrapper? This is not as simple as it seems at first glance. A _perfect_ C++ wrapper has to:

* Handle any kind and any number of C++ objects as input and as output.
* Perform perfect forwarding, that is, do not make unnecessary copies, be able to forward constants, do not add or remove references, take advantage of move semantics, etc.
* Handle all kind of "callable objects", such as class members, lambdas, function pointers.
* In summary, a wrapper that acts full transparently without adding any extra runtime code.

This could be such wrapper:

```cpp
template <typename callable, typename... callableArgs>
decltype(auto) throwIfFails(callable &&c, callableArgs &&... args) {
  return std::invoke(std::forward<callable>(c), std::forward<callableArgs>(args)...);
}
```

Well, do not feel intimidated. There are a few concepts here but they are not so hard to understand.
First, yes, it's *template function*, the correct term for this construction (it's not a class nor a variable, it's a free function with a template, therefore a template function).
Such function is also a *variadic template*, declared as:

```cpp
template <typename callable, typename... callableArgs>
```

which will templatize one argument and then, any number of arguments. First, `typename callable`, that will be anything with a type (ideally some callable object such a function or lambda, but for now it could be also a `int` or a `char` fits here), and then, any number of arguments also of any type.
The syntax `typename... callableArgs` indicates a *parameter pack*, a compile-time object that contains the remaining list of types used to instantiate this template.
To make things crystal clear, if our function to be wrapped is defined as:

```cpp
int my_legacy(int, float);
```

and we instanciate our wrapper as:

```cpp
throwIfFails( my_legacy, 2, 2.0f);
```

this template will be instanciated as:

```cpp
template <int(&)(int, float), int, float>
```

Note that the first parameter `int(&)(int, float)` is the type of the reference to `my_legacy`, and the remaining parameters, now expanded from the parameter pack, were deduced from the constants `2, 2.0f`. They are not yet related at all with the callable, they are just types all of them deduced by the compiler.

Now comes the function declaration itself, as:

```cpp
decltype(auto) throwIfFails(callable &&c, callableArgs &&... args)
```

The first interesting thing is `decltype(auto)`. It is deduced to be exactly the type of the returned value. Oh, but couldn't it simply be `auto`?
Apparently yes it could, but the devil is in the details. Look at this:

```cpp
object&  function (  // function that returns some reference

auto value = function (  // get the returned reference as auto
```

in that case, `value` will be deduced as `object`, not as `object&`. Thus, you are making an unnecessary copy. You could use instead:

```cpp
auto& value = function ( // get the returned reference as auto &
```

and now we are keeping the reference because we are using ``auto &``. Ok, but in our universal perfect wrapper, should we use `auto` or `auto&`?. Neither of them, since C++ 14 we have ``decltype(auto)``. It is a placeholder type specifier that match exactly the deduced type.

Function arguments also need some explication:

```cpp
decltype(auto) throwIfFails(callable &&c, callableArgs &&... args)
```

why are we using `&&` for the arguments? This is a quite confusing syntax at first, because you may think that we are forcing the argument to be a R-Value reference. After all, if you declare

```cpp
void function (Object &);
```

you are forcing `Object` to be passed by reference and likewise, if you declare

```cpp
void function (Object &&);
```

you are forcing `Object` to be pased as R-Value. However, in a deduced-context within a template, `&&` adquires a special meaning. It will be deduced as `&&` if the object can be moved (because it's a R-Value, or the object has move semantics), but it will decay to `&` otherwise. This is what Scott Mayers called "universal reference" as a mnemonic, expressing that, in the context of a template, `&&` means "a reference to anything", either L-Value or R-Value.

At this point we can say that we have 'perfectly received' the parameters, now we need to forward them to our target callable:


```cpp
  return std::invoke(std::forward<callable>(c), std::forward<callableArgs>(args)...);
```

We are using `std::invoke`, a standard template for invoking anything that could be invocable (a function reference, a lambda, a  `std::function`, a function class member, whatever).
For its part, `std::forward` is also a standard helper that will perfectly forward the parameters. There is legitimate doubt about this, however. If we had already the parameters 'perfectly received', why do we need to use `std::forward` and not simply:


```cpp
  return std::invoke(c, args...);
```

it will also work. However there are cases where this recipe would be sub-optimal: if we are taking an R-Value reference, for example from a temporal object, and we forward it to a function that only takes L-Values, the compiler has to defensively assume that you might be accessing it again after the call to `std::invoke`, and chose the lvalue reference version instead, therefore making a copy. Probably the optimizer will remove the copy anyway, but it expresses better our intention and it is more correct to use the standard `std::forward`; it is like saying: "We are not going to use this anymore, pass it through as  `&&` if possible because we don't care if it will become empty afterward"

Lastly, we have the `...` again for `std::forward<callableArgs>(args)...` . In this context `...` simply means "repeat the argument at the left until all the parameter pack is consumed", so if we have two arguments, it is equivalent to:


```cpp
  return std::invoke(std::forward<callable>(c), 
     std::forward<callableArgs>(arg_0),  
     std::forward<callableArgs>(arg_1));
```

simply like that. Finally, we have our "perfect C++ wrapper" explained. Don't worry if you didn't understand 100%, there are many advanced concepts in such little piece of code, but for sure you got the main idea. Let's imagine how it would be compiled:

If we have the following code

```cpp
#include <functional>

int my_legacy(int a, int b){
  return a+b;
}

template <typename callable, typename... callableArgs>
decltype(auto) throwIfFails(callable &&c, callableArgs &&... args) {
  return std::invoke(std::forward<callable>(c), std::forward<callableArgs>(args)...);
}

int main (int argc, const char *argv[]){
  return throwIfFails(my_legacy, argc,argc+1);
}
```

Passing `argc` is just a trick to provide some unknown variable and that way we prevent the optimizer to eliminate the whole code. That code will instantiate our wrapper as:

```cpp
template<>
int throwIfFails<int (&)(int, int), int &, int>(int (&c)(int, int), 
   int & args, int && __args2)
{
  return std::invoke(std::forward<int (&)(int, int)>(c), 
     std::forward<int &>(args), std::forward<int>(__args2));
}
```

and will generate the following assembly code: -> [GodBolt](https://godbolt.org/z/Vr3Yd6)

```
my_legacy(int, int):
  lea     eax, [rdi+rsi]
  ret
main:
  lea     esi, [rdi+1]
  call    my_legacy(int, int)
  ret
```

yes, the compiler understood completely our intention, and it generated the minimum possible code: same instructions that a direct call and same that a hand-crafted assembly program. It's effectively a perfect, transparent wrapper.
This also shows that, although the template code is a bit cumbersome and can intimidate us, they are just instructions for the compiler and the generated code doesn't get bloated, on the contrary, it allows to move computation logic from runtime to build time, helping us to optimize our binaries.

You can also be thinking that we are creating a wrapper to calling legacy C code, and such code can't have move semantics, objects, etc, just plain types and pointers, so there was no point in bringing all this stuff.
And yes, you are right, if you are sure that only POD (Plain-Old-Data) types will be used, this alternative template could also do the job:

```cpp
template <typename callable, typename... callableArgs>
auto throwIfFails(callable &c, callableArgs &&... args) {
  return c(args...);
}
```
but what are you truly gaining by removing `decltype(auto)`, `std::invoke` and `std::forward`?  The generated code, as we have demonstrated, will be the same if these templates aren't necessary, but if for some reason they need to act, they are better there. Also, there is a good point in understanding the idioms behind a perfect C++ wrapper, and that is a useful thing itself. You can always take this concept in the future if you need a C++ wrapper for any case.



## Capturing the result value

The above wrapper could be already useful by putting some code before the invocation to use the received arguments, but for our purpose, we need first intercept the value returned by the callable. Well, it is not that complicated:

```cpp
template <typename callable, typename... callableArgs>
decltype(auto) throwIfFails(callable &&c, callableArgs &&... args) {
  decltype(auto) result = std::invoke(std::forward<callable>(c), 
     std::forward<callableArgs>(args)...);
  return result;
}
```

we have again `decltype(auto)` to automatically declare  `result ` as the returned type by `std::invoke`, so this is quite simple.
Funny things start when we want to discriminate `result` by type and check it has been succeeded. There is no standard protocol for returning errors, it depends on the library convention, so we are going to finally write code for GLib. Most GLib-based functions return error by indicating ``FALSE`` in a `gboolean` type, or by returning a `NULL` pointer of some type.
We can try with this template helper that returns `true` or `false` depending of the result type:

```cpp
template <typename ReturnValue>
inline std::enable_if_t<std::is_pointer_v<ReturnValue>, bool>
isReturnGood(ReturnValue r) {
  return r != nullptr;
}

template <typename ReturnValue>
inline std::enable_if_t<std::is_same_v<ReturnValue, gboolean>, bool>
isReturnGood(ReturnValue r) {
  return r == 1;
}
```

this is pretty and elegant, but also an absolute mess if we don't understand the idioms behind it. Indeed, it is quite ugly until we don't get it, to be honest.
Let's descompose this a bit, starting by the first template. The standard `std::is_pointer_v` will evaluate to `std::true_type` if the parameter type is a pointer (`void*`, `char*`, etc). It's actually a shortcut to `std::is_pointer<ReturnValue>::value`. There is not much mistery here, let's test it:

````cpp
#include <iostream>
#include <type_traits>

int main() {
  std::cout << std::is_pointer_v<char*> << ",";
  std::cout << std::is_pointer_v<char> << ",";
  std::cout << std::is_pointer_v<void*> << ",";
  std::cout << std::is_pointer_v<bool> << "\n";
  return 0;
}
````

this will print `1,0,1,0`. The tricky part comes with `std::enable_if_t` (sugar sintax to `std::enable_if<  >::type`) because it looks evident, but it uses a very important idiom in template metaprogramming: _SFINAE_.
_SFINAE_ stands for "substitution failure is not an error", and we can simplify the concept understanding that if a template is well-formed but it can't be instantiated, because it doesn't fit with the passed parameters, it will be discarded and the compiler will search for a better match.
How does `std::enable_if_t<std::is_pointer_v<ReturnValue>, bool>` work? It can take two possible values, depending of wheter `std::is_pointer_v<ReturnValue>` evaluates to `std::true_type` or `std::false_type`.

If it is `std::enable_if_t<std::true_type, bool>`, then it will evaluate to the type of its second parameter, `bool`.
But if it is `std::enable_if_t<std::false_type, bool>`, then the `std::enable_if` implementation will use _SFINAE_ to fail the instantiation apropos, and this template will be discarded without error.
Note that we are using all that verbose template `std::enable_if_t<std::is_pointer_v<ReturnValue>, bool>` at the place of the return type in our function, so if we instantiate our template for example with `isReturnGood<void*>` the code will be resolved as:

````cpp
inline bool isReturnGood(void* r) {
  return r != nullptr;
}
````

and if we pass something like `isReturnGood<char>` it simply won't exist. Nice, isn't?
Now the second version of the template should be a piece of cake for you: `std::is_same_v<ReturnValue, gboolean>` will evaluate to `std::true_type` if `ReturnValue` has the same type as `gboolean`, and in such case the code will be instanciated as:

```cpp
inline bool isReturnGood(gboolean r) {
  return r == 1;
}
```

ok, crystal clear. But why `r == 1` and not simply `return r;` ? Well, bad news here. Sadly `gboolean` is defined in GLib as :

```cpp
typedef int gint;
typedef gint gboolean;
```

that is, its type is `int`, and not `bool`, how a C++ mind would expect. This is because it was defined before C99, but for our matters, this is a good opportunity to remember that `typedef` and `using` do not create new types, just aliases to existing ones.
This creates a security hole in our type-safe code, because there is no mechanism in the language to distinguish between`gboolean` and `int`, they are the same type to all intents and purposes.
The only thing that we can do is `return r == 1` to avoid the warning about the implicit conversion from `int` to `bool`. We could put some assertions like `assert( r == 0 ||r == 1)` to catch incorrect values from a GLib function returning `int` type with another error semantics, but the root problem is that we can't handle `int` and `gboolean` as different things. Fortunately, most of all GLib functions seems to adhere to the same convention, except those returning handles such as `g_file_open`. We can take care of these exceptions introducing some template parameters, but to not complicate too much the code, we'll ignore this for the moment.  Anyway, this is a good example of the importance of choosing the right types in our C++ code.

With these considerations, we can finally check if our callable is returning a good result or not:

```cpp
template <typename ReturnValue>
inline std::enable_if_t<std::is_pointer_v<ReturnValue>, bool>
isReturnGood(ReturnValue r) {
  return r != nullptr;
}

template <typename ReturnValue>
inline std::enable_if_t<std::is_same_v<ReturnValue, gboolean>, bool>
isReturnGood(ReturnValue r) {
  return r == 1;
}

template <typename callable, typename... callableArgs>
decltype(auto) throwIfFails(callable &&c, callableArgs &&... args) {
  decltype(auto) result = std::invoke(std::forward<callable>(c), 
  std::forward<callableArgs>(args)...);

  if (!isReturnGood(result) ){
   // Throw some exception here
   throw std::runtime_error("legacy failure");
  }
 return result;
}
```
So far, so good. We have a C++ wrapper that handles errors from GLib in a C++ way, but it is not that impressive if we stop here.


## Handling GError**


Please take a breath. If you have understood until this point without previous knowledge about template metaprogramming, you are some kind of genius. Even a seasoned C++ programmer would need some time to truly interiorize the idioms described.
Anyway, if you decide to keep reading, the really interesting thing comes now, because we have promised to catch a `GError**` parameter, which can be, or not, somewhere in the argument list. Interesting stuff.

First, what's a `GError**` parameter? It's a pointer-to-pointer-to C struct that contains additional information about the failure. A GLib function my operate this way:

```cpp
gboolean
my_function_that_can_fail (GError **err)
{
// some code here
  if (some_condition_fails)
    {
      g_set_error (error,
        FOO_ERROR,                 // error domain
        FOO_ERROR_BLAH,            // error code
        "Function failed %d", // error message format string
        g_strerror (saved_errno));
      return -1;
    }

  // otherwise continue, no error occurred
}
```

and the client code can use it like:

```cpp
GError *err=nullptr;
gboolean result =  my_function_that_can_fail (&err);
if (!result){
  //Get information in err
  //...
  //Free it
  g_error_free(err);
  }
```

we need to detect if some of the parameters are `GError`, and if found one, we need to implement the corresponding logic within the wrapper to throw a fancy exception.
Let's take a look to our wrapper again:

```cpp
template <typename callable, typename... callableArgs>
decltype(auto) throwIfFails(callable &&c, callableArgs &&... args) {
   // irrevant code here...
}
```

it has to be a way to check if some argument in `...args` is of some type, right? Yes, there are several ways, but we are doing it wrong. Remember how the template was instantiated at point 3.1. Ok, don't worry, let's say that we instantiate the wrapper with that `gboolean my_function_that_can_fail (GError **err)`, for example:


```cpp
throwIfFails(my_function_that_can_fail, nullptr);
```

we will get:

```cpp
template<>
int throwIfFails<int (&)(GError **), nullptr_t>(int (&c)(GError **), nullptr_t && __args1)
{
 // irrevant code here...
}
```

ouch! None of the arguments passed to the template is `GError**`. We have the reference to the callable, which is `int (&c)(GError **)`, and the argument, which is `nullptr_t`. Do you remember that each one of the arguments was deduced separately? In our template, as defined, nothing indicates to the compiler that the second argument "has to be" `GError**` instead of `nullptr`, which is what we have passed.

So the type `nullptr_t` of our second argument may be useful, but right now what we need is to look for the type within the callable signature, not in the template variadic arguments.
Metaprogramming to the rescue of the metaprogramming issue:

```cpp
template <typename Type> struct function_traits;

template <typename returnValue, typename... callableArgs>
struct function_traits<returnValue (&)(callableArgs...)> {
  using arg_tuple = std::tuple<callableArgs...>;
};
```

this is our first example of a "meta-function", defined more or less like such helpers as `std::is_pointer` that we have seen before. First, we have:

```cpp
template <typename Type> struct function_traits;
```

which is nothing, just a primary template of a struct with one template argument. Its only purpose is that it allows to specialize it:

```cpp
template <typename returnValue, typename... callableArgs>
struct function_traits<returnValue (&)(callableArgs...)> {
  using arg_tuple = std::tuple<callableArgs...>;
};
```

Do you remember our former callable's signature, `int (&)(GError **)` ? Look how well it fits into: `struct function_traits<returnValue (&)(callableArgs...)>`.
So if we instantiate it as:

```cpp
   ... throwIfFails(callable &&c, callableArgs &&... args) {
   ... function_traits<callable>
}
```

the compiler will find the perfect match in the template specialization and we'll get `function_traits<callable>::arg_tuple` evaluated as a `std::tuple`  corresponding with the callable's signature arguments, so:

```function_traits<callable>``` => _evaluates to_

```function_traits<int (&)(GError **)>::arg_tuple``` => _evaluates to_

```std::tuple<GError **>```


we are basically extracting the arguments from the callable signature and converting them in the equivalent `std::tuple`, which is much more "playable" for the remaining operations. Wow, nice trick.
Ok, so we ended with a `std::tuple` looking like something as `std::tuple<int, float, GError **, char>`. How do we find if `GError**` is inside?
This is another meta-function for that:


```cpp
template <typename Type, typename Tuple> struct tuple_has_type;

template <typename Type, typename... tupleArgs>
struct tuple_has_type<Type, std::tuple<tupleArgs...>>
  : std::disjunction<std::is_same<Type, tupleArgs>...> {};
```

as in the previous meta-function, the first template `template <typename Type, typename Tuple> struct tuple_has_type` is the primary template, this time with two parameters, just to provide a base to be specialized.
For the heart template, we'll need some attention:

```cpp
template <typename Type, typename... tupleArgs>
struct tuple_has_type<Type, std::tuple<tupleArgs...>> :
   std::disjunction<std::is_same<Type, tupleArgs>...> {};
```

The first part is just an `struct` that has to match with some type and some `std::tuple`, for example a valid instantiation could be: `struct tuple_has_type<GError**, std::tuple<int, float, GError**>>`
The hot part is that the struct is *inheriting* `std::disjunction<std::is_same<Type, tupleArgs>...>`

Let's expand it for the same example:

```cpp
template <GError**, std::tuple<int, float, GError**>
struct tuple_has_type<GError**, std::tuple<int, float, GError**>>   :
   std::disjunction<std::is_same<GError**, int>, 
     std::is_same<GError**, float>, std::is_same<GError**,GError**>> {};
```

at let's evaluate all these `std::is_same`

```cpp
template <GError**, std::tuple<int, float, GError**>
struct tuple_has_type<GError**, std::tuple<int, float, GError**>>   :  
  std::disjunction<std::false_type, std::false_type, std::true_type>> {};
```

and because `std::disjunction` forms the logical disjunction of its arguments, that's to say an OR operation, `std::disjunction<std::false_type, std::false_type, std::true_type>` will evaluate ` std::true_type` so:


```cpp
template <GError**, std::tuple<int, float, GError**>
struct tuple_has_type<GError**, std::tuple<int, float, GError**>>   :  std::true_type {};
```

let's test it:


```cpp

#include <iostream>
#include <type_traits>
#include <tuple>

template <typename Type, typename Tuple> struct tuple_has_type;

template <typename Type, typename... tupleArgs>
struct tuple_has_type<Type, std::tuple<tupleArgs...>>
  : std::disjunction<std::is_same<Type, tupleArgs>...> {};

int main() {

    std::cout << tuple_has_type < GError**, std::tuple < GError**, int > >::value << ",";
    std::cout << tuple_has_type < GError**, std::tuple < float, int > >::value << ",";
    std::cout << tuple_has_type < GError**, std::tuple < char, GError**> >::value << "\n";

    return 0;
}
```

this will print `1,0,1` as expected.


## Putting all together

Now that we have all the interesting meta-programming resolved, the remaining work is putting all the pieces together with ordinary C++ code, for example:


```cpp

template <typename ReturnValue>
inline std::enable_if_t<std::is_pointer_v<ReturnValue>, bool>
isReturnGood(ReturnValue r) {
  return r != nullptr;
}

template <typename ReturnValue>
inline std::enable_if_t<std::is_same_v<ReturnValue, gboolean>, bool>
isReturnGood(ReturnValue r) {
  return r == 1;
}

[[maybe_unused]] static void throw_using_gerror_and_free(GError *error) {
  if (error != nullptr) {
    GErrorException except{ error->domain, error->code, error->message };
    g_error_free(error);
    throw except;
    }else {
    throw std::runtime_error("Legacy funtion failed");
    }
}

template <typename callable, typename... callableArgs>
decltype(auto) throwIfFails(callable &&c, callableArgs &&... args) {

    using argsAsTuple = typename fmpl::function_traits<callable>::arg_tuple;

    if
        constexpr (tuple_has_type<GError **, argsAsTuple>::value) {
        argsAsTuple tuple(args...);
        GError **gerror = std::get<GError **>(tuple);

        assert(gerror == nullptr);

        GError *_g = nullptr;
        gerror = &_g;
        std::get<GError **>(tuple) = gerror;
        decltype(auto) result = std::apply(std::forward<callable>(c), tuple);
        if (!isReturnGood(result)) {
            throw_using_gerror_and_free(*gerror);
        }
        return result;
    }
    else {
        decltype(auto) result = std::invoke(std::forward<callable>(c), 
            std::forward<callableArgs>(args)...);
        if (!isReturnGood(result)) {
            throw_using_gerror_and_free(nullptr);
        }
        return result;
    }

}
```

there are still some things to explain about this code. First is that we are using C++ `if constexpr` to directly decide, at build time, if we are going to inject the code for `GError**` handling or not, depending on the caller signature. This can be implemented in previous C++ standards using different template versions with ``std::enable_if``, as we have seen before, but this is a good opportunity to show you this alternative C++17 code to achieve the same effect.

The other interesting point is `std::apply`, also C++17, that directly invokes the callable, much like `std::invoke`, but passing a `std::tuple` decomposed as function arguments. We can again implement an equivalent template for previous C++ versions, but C++17 has it already done.
A last little detail is that we are also implementing `throw_using_gerror_and_free` as a static helper, because it is in the slow path (only when an exception is going to be thrown), and it's better do not repeat it to avoid code bloating (the optimizer will be probably compact it anyway, but the code has to be, first, correct, and then, the optimizer will do its job).


## The namespace sugar syntax

I almost forgot I have promised that the syntax to use the helper will be something like

```cpp
gchar *result = ThrowIfFail::my_legacy_one(0, 0, nullptr);
```

but with the previous code, it has to be used as

```cpp
gchar *result = throwIfFails(my_legacy_one, 0, 0, nullptr);
```

well, to achieve this we can use our well-beloved `#define` macro.
There is not yet, as far as I know, another C++ feature to programmatically create functions with a given name:


```cpp
#define GLIB_THROW_IF_FAILS(function)                                          \
  namespace ThrowIfFail {                                                      \
  template <typename... callableArgs>                                          \
  inline decltype(auto) function(callableArgs &&... args) {                    \
    return ::throwIfFails(::function, std::forward<callableArgs>(args)...);    \
  }                                                                            \
  }
```

and we can simply declare the wrapper with:

```cpp
GLIB_THROW_IF_FAILS(my_legacy_one)
```

if you have read to this point, you should understand perfectly that easy-peasy code.


# Final implementation

This basic implementation may be, and in fact, it has been, refined and twisted to experiment and to improve compile checking. For example, we can assure that the user is passing `nullptr` for `GError**` at compile time instead of a runtime (the runtime assert will be optimized at release, but the compile-time version gives instantaneous feedback), and some other improvements.
But this is basically a repetition of the same basic techniques, and for the goals of this TL;DR document, the basics explained are enough.

You can find the full implementation, with more information, unit tests, etc, at: [BitBucket](https://bitbucket.org/nachogarglez/cpp_experimental/src/master/)

I also split the useful meta-function in a proto-library for meta-programming that we could use someday.
Please feel free to contribute to this repository, make comments, and above all, have some fun!
Modern C+++ template meta-programming is one of the more evolving areas in the language, a little hard to understand at first but that worths learning.
