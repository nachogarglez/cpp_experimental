# legacy_throw

Wrapper around legacy GLIB stuff using 
Fluendo's Metaprogramming Library. 

This wrapper automatically throws an exception
if a legacy GLib-based function returns an error.
It also automatically detects if the legacy functions
has a GError** parameter, and it is able to fill 
the corresponding structures and return the error information
in the C++ exception.

## How to use

Given a legacy function such as:

```cpp
gchar*  my_legacy (int param1, GError** gerror, int param2)
```

wrap it with the provided macro

```cpp
GLIB_THROW_IF_FAILS (my_legacy)
```

and you will be able to use the throwing-version in the ThrowIfFail
namespace, this way:

```cpp
gchar *result = ThrowIfFail::my_legacy(0, nullptr, 0);
```

The GError** information is automatically filled and handled by the wrapper
and an exception will throw in case of failure.


Have fun
