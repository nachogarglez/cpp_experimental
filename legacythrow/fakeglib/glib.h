/*
  This is a Fake GLib's GError implementation
  just to avoid GLIB dependency for tests in
  Visual Studio, etc
  Not real application, should be removed
  in production code
*/

#pragma once
#include <cstdint>
#include <cstdlib>
#include <cstring>

#ifdef _MSC_VER
#define POSIX_STRDUP _strdup
#else
#define POSIX_STRDUP strdup
#endif

typedef uint32_t GQuark;
typedef char gchar;
struct GError {
  GQuark domain;
  int code;
  char *message;
};
GQuark g_quark_from_static_string(const gchar *string) {
  (void)string;
  return 0xDEADBEEF;
}
typedef int gboolean;
GError *g_error_new(GQuark domain, int code, const char *format) {
  (void)domain;
  GError *error = new GError;
  memset(error, 0, sizeof(GError));
  error->message = POSIX_STRDUP(format);
  error->code = code;
  return error;
}
void g_error_free(GError *error) {
  free(error->message);
  delete error;
}
