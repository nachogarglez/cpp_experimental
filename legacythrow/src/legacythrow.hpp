/*

Wrapper around legacy GLIB stuff using
Fluendo's Metaprogramming Library.

This wrapper automatically throws an exception
if a legacy GLib-based function returns an error.
It also automatically detects if the legacy functions
has a GError** parameter, and it is able to fill
the corresponding structures and return the error information
in the C++ exception.

How to use

Given a legacy function such as:

gchar*  my_legacy (int param1, GError** gerror, int param2)

wrap it with the provided macro

GLIB_THROW_IF_FAILS (my_legacy)

and you will be able to use the throwing-version in the ThrowIfFail
namespace, this way:

gchar *result = ThrowIfFail::my_legacy(0, nullptr, 0);

The gerror information is automatically filled and handled by the wrapper
and an exception will throw in case of failure.

*/
#pragma once

#include <functional>
#include <stdexcept>
#include <glib.h>
#include <fmpl/function_traits.hpp>
#include <fmpl/tuple_types.hpp>
#include "config.h"
#ifdef _MSC_VER
#include <cassert>
#endif

#if CPP_STANDARD < CPP_STANDARD_17
#error Need C++17 or higher
#endif

struct GErrorException : public std::runtime_error {
	GErrorException(GQuark _domain, int _code, std::string _msg)
		: runtime_error(_msg), domain(_domain), code(_code), msg(_msg) {}
	GQuark domain;
	int code;
	std::string msg;
};

template <typename ReturnValue>
inline std::enable_if_t<std::is_pointer_v<ReturnValue>, bool>
isReturnGood(ReturnValue r) {
	return r != nullptr;
}

template <typename ReturnValue>
inline std::enable_if_t<std::is_same_v<ReturnValue, gboolean>, bool>
isReturnGood(ReturnValue r) {
	return r == 1;
}

[[maybe_unused]] static void throw_using_gerror_and_free(GError *error) {
	if (error != nullptr) {
		GErrorException except{ error->domain, error->code, error->message };
		g_error_free(error);
		throw except;		
	}
	else {
		throw std::runtime_error("Legacy funtion failed");
	}
}


template <typename callable, typename... callableArgs>
decltype(auto) throwIfFails(callable &&c, callableArgs &&... args) {

	using argsAsTuple = typename fmpl::function_traits<callable>::arg_tuple;
	static_assert(sizeof...(callableArgs) == std::tuple_size<argsAsTuple>::value,
		"Incorrect number of arguments");
	if
		constexpr (fmpl::tuple_has_type<GError **, argsAsTuple>::value) {
#ifndef _MSC_VER //Note 1
		using GErrorType = typename fmpl::get_type_by_position<
			fmpl::get_tuple_type_position<GError **, argsAsTuple>::value,
			callableArgs...>::type;
		static_assert(std::is_same_v<GErrorType, decltype(nullptr)>,
			"GError parameter must be nullptr");
#endif
		argsAsTuple tuple(args...);
		GError **gerror = std::get<GError **>(tuple);
#ifdef _MSC_VER
		assert(gerror == nullptr);
#endif
		GError *_g = nullptr;
		gerror = &_g;
		std::get<GError **>(tuple) = gerror;
		decltype(auto) result = std::apply(std::forward<callable>(c), tuple);
		if (!isReturnGood(result)) {
			throw_using_gerror_and_free(*gerror);
		}
		return result;
	}
	else {
		decltype(auto) result = std::invoke(std::forward<callable>(c), std::forward<callableArgs>(args)...);
		if (!isReturnGood(result)) {
			throw_using_gerror_and_free(nullptr);
		}
		return result;
	}
}

#define GLIB_THROW_IF_FAILS(function)                                          \
                                                                               \
  namespace ThrowIfFail {                                                      \
  template <typename... callableArgs>                                          \
  inline decltype(auto) function(callableArgs &&... args) {                    \
    return ::throwIfFails(::function, std::forward<callableArgs>(args)...);    \
  }                                                                            \
  }

/*
Note1 :  At least Visual Sudio 2017 15.9 has a bug that causes the using declaration
to be avaluated even if "if constexpr" branch is not taken.
Probably resolved in Visual Studio 2019

The alternative assert will create the same effect but in runtime instead
of build time
*/