# cpp Experimental

Experiments in modern C++ that may or may not be useful . Just for fun.

## Papers

[Making Legacy Throw](https://bitbucket.org/nachogarglez/cpp_experimental/src/master/legacythrow/doc/making_legacy_throw.pdf)

## Compiler support 

Because this repo is for C++ experimentation, you'll need a modern compiler
in order to compile and run all tests.

Currently this has been successfully built with (please update this readme in the future)

* gcc 7.4.0 (default in Ubuntu Bionic 18.04 TLS)
* clang-6.0  (with gcc 7.4.0 library)
* clang-8 (with gcc 7.40 library)
* Visual Studio 2017, with some details

## Known compilers that fail

* gcc 6.3 (insufficient C++17 support)
* clang-3.8 (insufficient C++17 support)

## Testing framework

For now this repo is experimenting with [Catch2] (https://github.com/catchorg/Catch2) as native C++ testing framework.
  

## Meson and Visual Studio

Meson < 0.49 doesn't set /std:c++17 flag and has to be checked manually in the VS Project.
